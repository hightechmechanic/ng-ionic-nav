import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonApp, IonContent, IonIcon, IonTabBar, IonTabButton, IonTabs } from "@ionic/angular/standalone";
import { HomeComponent } from './page/home/home.component';

@Component({
	imports: [
		CommonModule,
		HomeComponent,
		IonApp,
		IonContent
	],
	selector: 'app-root',
	standalone: true,
	styleUrls: ['./app.component.scss'],
	templateUrl: './app.component.html'
})
export class AppComponent {
	title = 'Angular Ionic Nav';
}
