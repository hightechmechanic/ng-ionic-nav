import { Component } from '@angular/core';
import { IonButton, IonNavLink, IonTitle, IonHeader, IonBackButton, IonButtons, IonContent, IonToolbar } from "@ionic/angular/standalone";
import { NumerologyComponent } from '../numerology/numerology.component';
import { TarotComponent } from '../tarot/tarot.component';

@Component({
	imports: [
		IonBackButton,
		IonButton,
		IonButtons,
		IonContent,
		IonHeader,
		IonNavLink,
		IonTitle,
		IonToolbar
	],
	selector: 'app-xiangqi',
	standalone: true,
	styleUrls: ['./xiangqi.component.scss'],
	templateUrl: './xiangqi.component.html',
})
export class XiangqiComponent {

	numerologyComponent = NumerologyComponent;
	tarotComponent = TarotComponent;

	constructor() { }
}
