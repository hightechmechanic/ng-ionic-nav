import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { XiangqiComponent } from './xiangqi.component';

describe('XiangqiComponent', () => {
  let component: XiangqiComponent;
  let fixture: ComponentFixture<XiangqiComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [XiangqiComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(XiangqiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
