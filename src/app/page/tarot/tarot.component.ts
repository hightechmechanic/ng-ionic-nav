import { Component, OnInit } from '@angular/core';
import { IonContent, IonTitle, IonHeader, IonBackButton, IonButtons, IonButton, IonToolbar, IonNavLink } from "@ionic/angular/standalone";
import { NumerologyComponent } from '../numerology/numerology.component';
import { XiangqiComponent } from '../xiangqi/xiangqi.component';

@Component({
	imports: [
		IonBackButton,
		IonButton,
		IonButtons,
		IonContent,
		IonHeader,
		IonNavLink,
		IonTitle,
		IonToolbar
	],
	selector: 'app-tarot',
	standalone: true,
	styleUrls: ['./tarot.component.scss'],
	templateUrl: './tarot.component.html',
})
export class TarotComponent {

	numerologyComponent = NumerologyComponent;
	xiangqiComponent = XiangqiComponent;

	constructor() { }
}
