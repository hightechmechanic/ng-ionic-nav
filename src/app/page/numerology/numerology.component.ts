import { Component } from '@angular/core';
import { IonNavLink, IonHeader, IonToolbar, IonButtons, IonBackButton, IonTitle, IonContent, IonButton, IonTabs, IonTabBar, IonTabButton, IonIcon } from "@ionic/angular/standalone";
import { TarotComponent } from '../tarot/tarot.component';
import { XiangqiComponent } from '../xiangqi/xiangqi.component';

@Component({
	imports: [
		IonBackButton,
		IonButton,
		IonButtons,
		IonContent,
		IonHeader,
		IonNavLink,
		IonTitle,
		IonToolbar
	],
	selector: 'app-numerology',
	styleUrls: ['./numerology.component.scss'],
	standalone: true,
	templateUrl: './numerology.component.html',
})
export class NumerologyComponent {

	tarotComponent = TarotComponent;
	xiangqiComponent = XiangqiComponent;

	constructor() { }
}
