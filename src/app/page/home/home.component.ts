import { Component } from '@angular/core';
import { IonIcon, IonNav, IonTabBar, IonTabButton, IonTabs } from "@ionic/angular/standalone";
import { NumerologyComponent } from '../numerology/numerology.component';

@Component({
	imports: [
		IonNav
	],
	selector: 'app-home',
	standalone: true,
	styleUrls: ['./home.component.scss'],
	templateUrl: './home.component.html'
})
export class HomeComponent {

	component = NumerologyComponent;

	constructor() { }
}
